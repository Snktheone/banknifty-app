### Bank Nifty App 
#### App Features 

* Backtesting and analysis for Bank Nifty Equities Derviatives 
* Tick Analysis
* Predictions on the biggest spikes in prices in a single 5 min interval 
* Automatic exit and entry bands on bullish and bearish bands on daily trading
* Special markers for major sociopolitical events especially for India
* Special markets for RBI guidelines and Twitter news thread impacting the Banking domain 
* AOB

#### Process to release new feature 

#### Process to test features

#### Process to test features with full data/sample data 

#### Process to run the app locally

### Technology Stack 

* python 3.8
* html5
* pex
* ES6
* npm
 


